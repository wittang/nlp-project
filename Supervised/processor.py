import numpy as np
import pandas as pd
import xlrd
import random

from sklearn.feature_extraction import text 
from sklearn import metrics

pos_Excel = "/Users/Wilson/Desktop/nlp-project/Data/PosSentiment.xlsx"
neg_Excel = "/Users/Wilson/Desktop/nlp-project/Data/NegSentiment.xlsx"
inq_Excel = "/Users/Wilson/Desktop/nlp-project/Data/Inquiries.xlsx"
weights_Path = "../Results/Weights/"
predictions_Path = "../Results/Predictions/"


my_stop_words = ["consumer", "product", "products", "coupons", "16", "15", "10", "17", "11", "12","2015"]
stop_words = text.ENGLISH_STOP_WORDS.union(my_stop_words)

excel_Paths = {'pos':pos_Excel, 'neg':neg_Excel, 'inq':inq_Excel}

def split_List(list, multiple):
    new_List = []
    length = len(list)/multiple
    for index in range(multiple):
        if index < multiple - 1:
            new_List.append(list[(index*length):(index+1)*length])
        else:
            new_List.append(list[(index*length):])
    return new_List

def getSets(subsets, index):
    train_Set = []
    test_Set = []
    for i in range(len(subsets)):
        if i != index:
            for comment in subsets[i]:
                train_Set.append(comment)
        else:
            for comment in subsets[i]:
                test_Set.append(comment)
    return train_Set, test_Set

def getSubsets(sentimenet_Type):
	path = excel_Paths[sentimenet_Type]
	data = pd.read_excel(path)
	comments = data["Comment"].values.astype('U')
	random.shuffle(comments)
	subsets = split_List(comments, 5)
	return subsets

def update_All(index, comments, data, labels):
	sentiment_Labels = ['pos', 'neg', 'inq']
	for i in range(len(sentiment_Labels)):
		data.append(comments[i][index])
		labels.append(sentiment_Labels[i])

def update_Corpora(index, train_Comments, train_Corpora):
	for i in range(len(train_Corpora)):
		train_Corpora[i].append(train_Comments[i][index])

# Create feature vector
# min_df = 5, discard words appearing in less than 5 documents
# max_df = 0.8, discard words appearing in more than 80% of documents
def getVectorizer():
	return text.TfidfVectorizer(min_df=5, max_df = 0.8, sublinear_tf=True, use_idf=True, stop_words= my_stop_words)

def getWeights(classifier, vectorizer, sentiment_Corpus):
	sentiment_Paths = ["Positive/", "Negative/", "Inquiry/"]
	for index in range(len(sentiment_Corpus)):
		weights = findWeights(vectorizer, sentiment_Corpus[index])
		writeWeight(weights, classifier, sentiment_Paths[index])


def findWeights(vectorizer, corpus):
	tvec_weights = vectorizer.fit_transform(corpus)
	weights = np.asarray(tvec_weights.mean(axis=0)).ravel().tolist()
	weights_df = pd.DataFrame({'term': vectorizer.get_feature_names(), 'weight': weights})
	weights_df.sort_values(by='weight', ascending=False, inplace=True)
	return weights_df
        
def writeWeight(weights, classifier, sentiment_Path):
	with pd.option_context('display.max_rows', None, 'display.max_columns', 3):
		with open(weights_Path + sentiment_Path + classifier + ".csv", "w") as file: 
			weights.to_csv(file, header=["Word", "Probability"], index = False)


def getResults(classifier, test_data, prediction):
	results_File = open(predictions_Path + classifier + "_Predictions.txt", "w")
	for index in range(len(test_data)):
		results_File.write(test_data[index].encode('utf-8') + '\t' + prediction[index].encode('utf-8') + '\n')
	results_File.close()

def printPerformance(classifier, time_train, time_predict, test_labels, prediction):
	print("Training time: %fs;\tPrediction time: %fs" % (time_train, time_predict))
	print(metrics.classification_report(test_labels, prediction))




import time
import processor

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn import svm

if __name__ == '__main__':
    classifiers = {
        'SVC(kernel=rbf)' : svm.SVC(), 
        'Linear_SVC': svm.SVC(kernel='linear'),  
        'SVC(kernel=linear)' : svm.LinearSVC(),
        'LogisticRegression' : LogisticRegression()
        }

    for key in classifiers:
        classifier_String = key
        classifier = classifiers[key]
        print("Test for " + classifier_String)


        # Get all labeled subsets
        pos_Subsets = processor.getSubsets('pos')
        neg_Subsets = processor.getSubsets('neg')
        inq_Subsets = processor.getSubsets('inq')

        # 5-Fold Cross validation
        for index in range(5):
            print("\n--- Test " + str(index + 1) + " ---")

            # Get all Train and Test Comments
            pos_Comments_Train, pos_Comments_Test = processor.getSets(pos_Subsets, index)
            neg_Comments_Train, neg_Comments_Test = processor.getSets(neg_Subsets, index)
            inq_Comments_Train, inq_Comments_Test = processor.getSets(inq_Subsets, index)

            # Create necessary data structures used
            train_data, train_labels = [], []
            test_data, test_labels = [], []
            pos_corpus, neg_corpus, inq_corpus = [], [], []

            # Combine data structures for ease of updating
            train_Comments = [pos_Comments_Train, neg_Comments_Train, inq_Comments_Train]
            test_Comments = [pos_Comments_Test, neg_Comments_Test, inq_Comments_Test]
            train_Corpora = [pos_corpus, neg_corpus, inq_corpus] 

            
            # Update all Training data structures
            for i in range(len(pos_Comments_Train)):
                processor.update_All(i, train_Comments, train_data, train_labels)
                processor.update_Corpora(i, train_Comments, train_Corpora)
            
            # Update all Testing data structures
            for j in range(len(pos_Comments_Test)):
                processor.update_All(j, test_Comments, test_data, test_labels)

            # Tranform data using feature - TF-IDF
            vectorizer = processor.getVectorizer()
            train_vectors = vectorizer.fit_transform(train_data)
            test_vectors = vectorizer.transform(test_data)
            
            # Get TF-IDF weights
            processor.getWeights(classifier_String, vectorizer, [pos_corpus, neg_corpus, inq_corpus])
            
            # Predict using classifier
            time_Start = time.time()
            classifier.fit(train_vectors, train_labels)
            time_Train_Finish = time.time()
            prediction = classifier.predict(test_vectors)
            time_Predict_Finish = time.time()
            time_train = time_Train_Finish - time_Start
            time_predict = time_Predict_Finish - time_Train_Finish

            # Get Predictions
            processor.getResults(classifier_String, test_data, prediction)
            
            # Print Performance of Classifier
            processor.printPerformance(classifier_String, time_train, time_predict, test_labels, prediction)


            